angular.module('PromoApp')
.controller('MainPageCtrl',function($scope){

})
.config(function($stateProvider){
	$stateProvider
	.state('app.index', {
      url: "/index",
      views: {
        'menuContent' :{
          templateUrl: "templates/pages/main/index.tpl.html",
          controller: 'MainPageCtrl'
        }
      }
    });
})
;