module.exports = {
	vendors: [
		'vendor/angular/angular.js',
		'vendor/angular-ui-router/release/angular-ui-router.js',
		'vendor/angular-animate/angular-animate.js',
		'vendor/angular-sanitize/angular-sanitize.js',
		
		'vendor/ionic/js/ionic.js',
		'vendor/ionic/js/ionic-angular.js',
	]	
};