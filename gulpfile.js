var gulp = require('gulp')
  , fs  = require('fs')
  , es = require('event-stream')
  // , bower = require('bower')
  , sh = require('shelljs')
  , ngHtml2Js = require("gulp-ng-html2js")
  , buildCfg = require('./builder/build.config.js')
  , tasks = require('gulp-load-plugins')()
  , del = require('del')
  , runSequence = require('run-sequence');
;

var pkg = require('./package.json');

gulp.task('prepare-sass', function(done) {
  gulp.src('./src/scss/**/*.scss')
    .pipe(tasks.sass())
    .pipe(gulp.dest('./www/assets/css/'))
    .pipe(tasks.minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(tasks.rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/assets/css/'))
    .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});


gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});


gulp.task('clean',function(done){
  del(['./build','./www'], done);
});

gulp.task('commit-files', /*['bumpLoader'],*/ function () {
    var pkg = require('./package.json'),
        v = pkg.version,
        message = 'Release ' + v;

    return gulp.src('.').pipe(tasks.git.commit(message));
});

gulp.task('html2js',function(){
  return gulp.src('./src/app/**/*.tpl.html')
  .pipe(ngHtml2Js({
    moduleName : 'templates',
    prefix     : 'templates/'
  }))
  .pipe(concat('templates-app.js'))
  .pipe(gulp.dest('./build'));
});
// build
gulp.task('jshint',function(){
  return;
  // return gulp.src('./src/app/**/*.js')
    // .pipe(tasks.cache(tasks.jshint('./.jshintrc')))
    // .pipe(tasks.jshint.reporter('jshint-stylish'));
});

gulp.task('prepare-app',function(){
  return gulp.src('./src/app/**/*.js')
    .pipe(tasks.ngmin())
    .pipe(tasks.concat('app.js'))
    .pipe(gulp.dest('./build'));
});

gulp.task('prepare-template',function(){
  return gulp.src('./src/app/**/*.tpl.html')
     .pipe(ngHtml2Js({
        moduleName : 'templates',
        prefix     : 'templates/'
     }))
     .pipe(tasks.concat('templates-app.js'))
     .pipe(gulp.dest('./build'));
});

gulp.task('make-app',function(){
  var src = buildCfg.vendors.concat([
     './src/module.prefix',
      './build/app.js',    
      './build/templates-app.js',
      './src/module.suffix'
  ]);

  return gulp.src(src)
     .pipe(tasks.concat('app.js'))
     .pipe(gulp.dest('./www/assets/js'))
     .pipe(tasks.rename({ suffix: '.min' }))
     .pipe(tasks.cache(tasks.uglify()))
     .pipe(gulp.dest('./www/assets/js'))
  ;
});

gulp.task('compile',['clean'],function(done){
  runSequence(['prepare-app','prepare-template'],done);
});

gulp.task('build',['jshint','compile'],function(done){
  runSequence(['copy-asssets','prepare-sass','make-app'],done);
});


gulp.task('copy-asssets',function(){
  return es.concat(
    // gulp.src('./vendor/ionic/css/ionic.css').pipe(gulp.dest('./www/assets/css/'))
    // , gulp.src('./src/css/style.css').pipe(gulp.dest('./www/assets/css'))
    gulp.src('./vendor/ionic/fonts/**').pipe(gulp.dest('./www/assets/fonts/'))
    , gulp.src(['./src/index.html','./src/config.xml',]).pipe(gulp.dest('./www/'))
  );
});

gulp.task('help',function(){
  tasks.util.log('Tasks lists');
});

gulp.task('run-ripple',function(){
  return sh.exec('ripple emulate');
});

gulp.task('dev',function() {
  gulp.watch(['./src/**','./gulpfile.js'],{
    debounceDelay : 5 
  },['build']);
});

gulp.task('emulate',['build'],function(done){
  runSequence('run-ripple',done);
});